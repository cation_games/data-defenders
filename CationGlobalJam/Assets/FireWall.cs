﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

public class FireWall : MonoBehaviour
{
    private BoardManager _boardManager;

    public float SecondsToLive = 30;
    // Use this for initialization
    void Start()
    {
        _boardManager = FindObjectOfType<BoardManager>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = new Vector3(SecondsToLive / 8, 1.5f,2.2f);
        SecondsToLive -= 0.01f;
        if (SecondsToLive <= 0)
            Destroy(this.gameObject);
    }

    void OnTriggerEnter(Collider collision)
    {


        // monster switch - don't judge me :)
        switch (collision.transform.tag)
        {
            case "Worm":
                Destroy(collision.gameObject);
                _boardManager.UiManager.SetVirusDestroyed();
               
                break;
            case "Trojan":

                Destroy(collision.gameObject);
                _boardManager.UiManager.SetVirusDestroyed();
               
                break;
            case "Malware":

                Destroy(collision.gameObject);
                _boardManager.UiManager.SetVirusDestroyed();
              
                break;
          
        }
    }
}
