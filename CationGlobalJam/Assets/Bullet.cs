﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private BoardManager _boardManager;
    public float moveSpeed = 4;
    // Use this for initialization
    void Start()
    {
        _boardManager = FindObjectOfType<BoardManager>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(moveSpeed * Time.deltaTime, 0, 0);
        if (transform.position.x < -25 || transform.position.x > 40)
            Destroy(this.gameObject);
    }

    void OnTriggerEnter(Collider collision)
    {
       

        // monster switch - don't judge me :)
        switch (collision.transform.tag)
        {
            case "Worm":
                Destroy(collision.gameObject);
                _boardManager.UiManager.SetVirusDestroyed();
                Destroy(this.gameObject);
                break;
            case "Trojan":

                Destroy(collision.gameObject);
                _boardManager.UiManager.SetVirusDestroyed();
                Destroy(this.gameObject);
                break;
            case "Malware":

                Destroy(collision.gameObject);
                _boardManager.UiManager.SetVirusDestroyed();
                Destroy(this.gameObject);
                break;
            case "Spam":
                Destroy(collision.gameObject);
                _boardManager.UiManager.SetVirusDestroyed();
                Destroy(this.gameObject);
                break;
        }
    }
}
