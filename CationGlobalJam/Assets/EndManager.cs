﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndManager : MonoBehaviour
{

    public Text finalScore;
    public Text DataCorrupted;
    public Text VirusDefeated;
    public Text VirusPassed;

    public Animator button;

    private Survivor survivor;
    // Use this for initialization
    void Start ()
    {
        survivor = FindObjectOfType<Survivor>();
        StartCoroutine(CountUpFaster(finalScore, survivor.finalScore));
        StartCoroutine(CountUp(DataCorrupted, survivor.DataCorrupted));
        StartCoroutine(CountUp(VirusDefeated, survivor.VirusDestroyed));
        StartCoroutine(CountUp(VirusPassed, survivor.VirusPassed));
    }
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetButtonDown("JoystickX"))
	        QuitGame();
    }

    private IEnumerator CountUp(Text text, float max)
    {
        float count = 0;
        float increment = 0.1f;
        while (count < max)
        {
            count += Random.Range(1, 2);
            if (count > max)
                count = max;
            text.text = count.ToString();
            yield return new WaitForSecondsRealtime(increment);
            increment = increment * 0.90f;
          
        }
    }

    private IEnumerator CountUpFaster(Text text, float max)
    {
        float count = 0;
        float increment = 0.1f;
        while (count < max)
        {
            count += Random.Range(80, 125);
            if (count > max)
                count = max;
            text.text = count.ToString();
            yield return new WaitForSecondsRealtime(increment);
            increment = increment * 0.90f;
        }
        button.SetTrigger("Open");
    }

    public void QuitGame()
    {
        Debug.Log("QUIT");
        Application.Quit();

    }



}
