﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class BoardManager : MonoBehaviour
    {

        [SerializeField] private Lane _lanePrefab;
        [SerializeField] private GameObject _playerPrefab;
        [SerializeField] private int _numberOfLanes = 4;
        [SerializeField] private List<Lane> _lanes;
        private bool speeed;
        public UiManager UiManager;

        private GameObject _board;
        private GameObject _player;
   
        private float _offset;
        // Use this for initialization
        void Start ()
        {
            UiManager = FindObjectOfType<UiManager>();

            GenerateGame(_numberOfLanes);






        }
	
        // Update is called once per frame
        void Update () {
            if(Time.timeSinceLevelLoad >= 83)
                SceneManager.LoadScene(2);
            if (Time.timeSinceLevelLoad >= 46 && !speeed)
            {
                speeed = true;
                SpeedUpEVERYTHING();
            }
               
        }


        public void SpeedUpEVERYTHING()
        {
            var temp = FindObjectsOfType<DataScript>();
            foreach (var potato in temp)
            {
                potato.MoveSpeed = potato.MoveSpeed * 2;
            }
        }

        // Generates apropriate number of lanes then the board
        void GenerateGame(int laneNumber)
        {
            for (int i = 0; i < laneNumber; i++)
            {
                GenerateLane(i);
            }
            // GenerateBoard();
            GeneratePlayer();
        }

        // Generates a lane and offsets the next one
        void GeneratePlayer()
        {
            _player  = Instantiate(_playerPrefab, new Vector3(0, 0, _offset/2-2), transform.rotation);
            _player.gameObject.transform.SetParent(transform);
            var startingPosition = Random.Range(0, _numberOfLanes);
            _player.GetComponent<Player>().SetLane(_lanes[startingPosition], startingPosition);
        }

        // Generates a lane and offsets the next one
        void GenerateLane(int number)
        {
            var laneClone = Instantiate(_lanePrefab, new Vector3(0, 0, _offset), transform.rotation);
            laneClone.gameObject.transform.SetParent(transform);
            _lanes.Add(laneClone);
            laneClone.name = "Lane " + number;
            _offset += laneClone.transform.localScale.z;
        }

        // generate a plane the size of all the exisiting lanes
        void GenerateBoard()
        {
            _board = GameObject.CreatePrimitive(PrimitiveType.Cube);
            _board.transform.position = new Vector3(0, -1, _offset / 2 - _lanes[0].transform.localScale.y);
            _board.gameObject.transform.SetParent(transform);
            _board.transform.name = "Board";
            _board.transform.localScale = new Vector3(_lanes[0].transform.localScale.x+1, 1, _offset+1);
        }

        // Flip all lanes
        public void FlipBoard()
        {
            for (int i = 0; i < _numberOfLanes; i++)
            {
                FlipLane(i);
            }
        }

        // Flip a particular lane
        public void FlipLane(int laneNumber)
        {
            _lanes[laneNumber].ChangeDirection();
        }

        /// <summary>
        /// Return one lane up or just the current one oif there is no valid one
        /// </summary>
        /// <param name="currentLane"></param>
        public void GetNextLane(int currentLane)
        {
            if (currentLane + 1 < _lanes.Count)
                currentLane += 1;
            _player.GetComponent<Player>().SetLane(_lanes[currentLane], currentLane);
        }

        /// <summary>
        /// Return one lane down or just the current one oif there is no valid one
        /// </summary>
        /// <param name="currentLane"></param>
        public void GetPreviousLane(int currentLane)
        {
            if (currentLane - 1 >= 0)
                currentLane -= 1;
            _player.GetComponent<Player>().SetLane(_lanes[currentLane], currentLane);
        }
    }
}
