﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXManager : MonoBehaviour {

	public static AudioClip virusDestroyed, dataDestroyed, shieldUse, virusSuccess, moveSound, firewallUse, bulletUse, pickUpCollect;
	static AudioSource audioSrc;

	// Use this for initialization
	void Start () {
		virusDestroyed = Resources.Load<AudioClip> ("DestroyedVirus");
		dataDestroyed = Resources.Load<AudioClip> ("DestroyedData");
		shieldUse = Resources.Load<AudioClip> ("ShieldUse");
		virusSuccess = Resources.Load<AudioClip> ("VirusSuccess");
		moveSound = Resources.Load<AudioClip> ("PlayerMove");
		firewallUse = Resources.Load<AudioClip> ("FirewallUse");
		bulletUse = Resources.Load<AudioClip> ("BulletUse");
		pickUpCollect = Resources.Load<AudioClip> ("PickupCollect");



		audioSrc = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public static void PlaySound (string clip) {
	
		switch (clip) {
		case "DestroyedVirus":
			if (!audioSrc.isPlaying) {
				
			}
			audioSrc.PlayOneShot (virusDestroyed);
			break;

		case "DestroyedData":
			audioSrc.PlayOneShot (dataDestroyed);
			break;

		case "ShieldUse":
			audioSrc.PlayOneShot (shieldUse);
			break;

		case "VirusSuccess":
			audioSrc.PlayOneShot (virusSuccess);
			break;

		case "Move":
			audioSrc.PlayOneShot (moveSound);
			break;

		case "FirewallUse":
			audioSrc.PlayOneShot (firewallUse);
			break;

		case "BulletUse":
			audioSrc.PlayOneShot (bulletUse);
			break;

		case "PickupCollect":
			audioSrc.PlayOneShot (pickUpCollect);
			break;
		}
	}
}
