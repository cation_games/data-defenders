﻿using UnityEngine;

namespace Assets.Scripts
{
    // Your general item class
    public class DataScript : MonoBehaviour
    {
        public float MoveSpeed;
        private bool _isLeft;
        private int _laneId;


        // Use this for initialization
        void Start()
        {

        }



        // Update is called once per frame
        void Update()
        {
            transform.Translate(-MoveSpeed * Time.deltaTime, 0, 0);
            if(transform.position.x < -25 || transform.position.x > 40)
				
                Despawn();

        }

        public void SwapTrack(int laneId)
        {
            if (laneId == _laneId)
            {
                _isLeft = !_isLeft;
                gameObject.transform.Rotate(0.0f, 180.0f, 0.0f);
                gameObject.transform.localScale = new Vector3(1.0f, 1.0f, -1.0f);
            }
        }

        public void InitData(bool isLeft, int laneId)
        {
            _isLeft = isLeft;
            _laneId = laneId;

            //Debug.Log("ITEM SET:\nLane: " + _laneID + "\nDirection is Left: " + _isLeft);
        }

        public void Despawn()
        {
          //  Debug.Log("Block Deleted");

            Destroy(this.gameObject);

        }
    }
}
