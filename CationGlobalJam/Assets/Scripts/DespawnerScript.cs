﻿using UnityEngine;

namespace Assets.Scripts
{
    // Kill space for data
    public class DespawnerScript : MonoBehaviour
    {
        void OnCollisionEnter(Collision collision)
        {
            //Debug.Log("Bump Detected");
            //if (bump.gameObject.tag == "Data")
            //{
            //    Debug.Log("Block Delete Requested");
            //    bump.gameObject.GetComponentInChildren<DataScript>().Despawn();
            //}
            //if (bump.gameObject.tag == "Data")
            //{
            //    Debug.Log("Block Delete Requested");
            //    bump.gameObject.GetComponentInChildren<DataScript>().Despawn();
            //}
            //if (bump.gameObject.tag == "Data")
            //{
            //    Debug.Log("Block Delete Requested");
            //    bump.gameObject.GetComponentInChildren<DataScript>().Despawn();
            //}


            var boardManager = FindObjectOfType<BoardManager>();

            if (collision.transform.name == "ShieldedData")
            {
                collision.gameObject.GetComponentInChildren<DataScript>().Despawn();
                boardManager.UiManager.DataSurvived();
                return;
            }


            switch (collision.transform.tag)
            {
                case "FireWall":
                    collision.gameObject.GetComponentInChildren<DataScript>().Despawn();
                    break;
                case "DataShield":
                    collision.gameObject.GetComponentInChildren<DataScript>().Despawn();
                    break;
                case "Bullet":
                    collision.gameObject.GetComponentInChildren<DataScript>().Despawn();
                    break;
                case "Worm":
                    collision.gameObject.GetComponentInChildren<DataScript>().Despawn();
                    boardManager.UiManager.VirusPassed();
                    break;
                case "Trojan":
                    collision.gameObject.GetComponentInChildren<DataScript>().Despawn();
                    boardManager.UiManager.VirusPassed();
                    break;
                case "Malware":
                    collision.gameObject.GetComponentInChildren<DataScript>().Despawn();
                    boardManager.UiManager.VirusPassed();
                    break;
                case "Spam":
                    collision.gameObject.GetComponentInChildren<DataScript>().Despawn();
                    boardManager.UiManager.VirusPassed();
                    break;
                case "CatPhoto":
                    collision.gameObject.GetComponentInChildren<DataScript>().Despawn();
                    boardManager.UiManager.DataSurvived();
                    break;
                case "Like":
                    collision.gameObject.GetComponentInChildren<DataScript>().Despawn();
                    boardManager.UiManager.DataSurvived();
                    break;
                case "Email":
                    collision.gameObject.GetComponentInChildren<DataScript>().Despawn();
                    boardManager.UiManager.DataSurvived();
                    break;
                case "ShoppingCart":
                    collision.gameObject.GetComponentInChildren<DataScript>().Despawn();
                    boardManager.UiManager.DataSurvived();
                    break;
            
            }

            if (collision.gameObject.tag == "Data")
            {
                Debug.Log("Block Delete Requested");
                collision.gameObject.GetComponentInChildren<DataScript>().Despawn();
            }
        }
    }
}
