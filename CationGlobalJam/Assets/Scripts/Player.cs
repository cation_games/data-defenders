﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    public class Player : MonoBehaviour
    {
        public BoardManager _boardManager;
        [SerializeField] private float _playerSpeed = 0.5f;
        [SerializeField] private float _playerLaneSwitchingSpeed = 15;
        [SerializeField] private Lane _currentLane;
        [SerializeField] private int _currentLaneNumber;
        [SerializeField] private bool _shielded;
        [SerializeField] private bool _aiming;
        [SerializeField] private MeshRenderer _shield;
        [SerializeField] private GameObject _bullet;
        [SerializeField] private GameObject _firewall;
        public int Firewall;
        public int Bullet;
        public int DataShield;
        [SerializeField] private GameObject _aimLeft;
        [SerializeField] private GameObject _aimRight;
        [SerializeField] private GameObject _aimWallLeft;
        [SerializeField] private GameObject _aimWallRight;
        private Animator _animator;
        // Use this for initialization
        void Start ()
        {
            _boardManager = FindObjectOfType<BoardManager>();
            _animator = GetComponentInChildren<Animator>();
            _boardManager.UiManager.SetPowerUps(this);
        }
	
        // Update is called once per frame
        void Update ()
        {
        
            _shield.gameObject.SetActive(_shielded);
            float joystickH = Input.GetAxis("JoystickHorizontal");
            float joystickV = Input.GetAxis("JoystickVertical");
    
            // Move up a lane if you are at your target lane
            if (Input.GetButton("JoystickY") && !_shielded)
            {
                UseDataShield();

            }

            // Aim bullet
            if (Input.GetButton("JoystickX") && Bullet > 0)
            {
                _aiming = true;
                if (joystickH >= 0.5)
                {
                    _aimRight.SetActive(true);
                    _aimLeft.SetActive(false);
                }
                else if (joystickH <= -0.5)
                {
                    _aimRight.SetActive(false);
                    _aimLeft.SetActive(true);
                }
                else
                {
                    _aimRight.SetActive(true);
                    _aimLeft.SetActive(false);
                }
            }
            else
            {
                _aimRight.SetActive(false);
                _aimLeft.SetActive(false);
            }

            // Aim wall
            if (Input.GetButton("JoystickB") && Firewall > 0)
            {
                _aiming = true;
                if (joystickH >= 0.5)
                {
                    _aimWallRight.SetActive(true);
                    _aimWallLeft.SetActive(false);
                }
                else if (joystickH <= -0.5)
                {
                    _aimWallRight.SetActive(false);
                    _aimWallLeft.SetActive(true);
                }
                else
                {
                    _aimWallRight.SetActive(true);
                    _aimWallLeft.SetActive(false);
                }
            }
            else
            {
                _aimWallRight.SetActive(false);
                _aimWallLeft.SetActive(false);
            }

            // if aiming build wall
            if (Input.GetButtonUp("JoystickB") && _aiming)
            {
                _aiming = false;
                Firewall--;
                _boardManager.UiManager.SetFirewall(Firewall);
				//SFXManager.PlaySound ("FirewallUse");		new sound required, current one was based on a fire aesthetic
                if (joystickH >= 0.5)
                    Instantiate(_firewall, new Vector3(transform.position.x + 6, 0, _currentLane.transform.position.z), new Quaternion(0, 0, 0, 0));
                else if (joystickH <= -0.5)
                {
                    Instantiate(_firewall, new Vector3(transform.position.x - 6, 0, _currentLane.transform.position.z), new Quaternion(0, 180, 0, 0));
                }
                else
                    Instantiate(_firewall, new Vector3(transform.position.x + 6, 0, _currentLane.transform.position.z), new Quaternion(0, 0, 0, 0));
            }

            // If aiming fire bullet
            if (Input.GetButtonUp("JoystickX") && _aiming)
            {
                _aiming = false;
                Bullet--;
                _boardManager.UiManager.SetBullet(Bullet);
				SFXManager.PlaySound ("BulletUse");
                if (joystickH >= 0.5)
                    Instantiate(_bullet, transform.position, new Quaternion(0, 0,0,0));
                else if(joystickH <= -0.5)
                {
                    Instantiate(_bullet, transform.position, new Quaternion(0, 180, 0, 0));
                }
                else
                    Instantiate(_bullet, transform.position, new Quaternion(0, 0, 0, 0));
            }

            if (Input.GetKey(KeyCode.W) || joystickV <= -0.5)
            {
                if (Math.Abs(transform.position.z - _currentLane.transform.position.z) < 0.15f)
                {
                    _boardManager.GetNextLane(_currentLaneNumber);

                }
                else
                {
                    _animator.SetBool("Up", true);
                    _animator.SetBool("Down", false);
                    _animator.SetBool("Right", false);
                    _animator.SetBool("Left", false);
                }
	     
            }
	  
            //Move down a lane if you are at your target lane
            else if (Input.GetKey(KeyCode.S) || joystickV >= 0.5)
            {
                if (Math.Abs(transform.position.z - _currentLane.transform.position.z) < 0.35f)
                {
                    _boardManager.GetPreviousLane(_currentLaneNumber);
                }
                else
                {
                    _animator.SetBool("Up", false);
                    _animator.SetBool("Down", true);
                    _animator.SetBool("Right", false);
                    _animator.SetBool("Left", false);
                }
	      
            }
            if (Math.Abs(transform.position.z - _currentLane.transform.position.z) < 0.35f)
            {
              
                if ( joystickH >= 0.5)
                {
                    _animator.SetBool("Up", false);
                    _animator.SetBool("Down", false);
                    _animator.SetBool("Right", true);
                    _animator.SetBool("Left", false);
                }
                else if ( joystickH <= -0.5)
                {
                    _animator.SetBool("Up", false);
                    _animator.SetBool("Down", false);
                    _animator.SetBool("Right", false);
                    _animator.SetBool("Left", true);
                }
                else if(_currentLane.CurrentDirection == "Left")
                {
                    _animator.SetBool("Up", false);
                    _animator.SetBool("Down", false);
                    _animator.SetBool("Right", true);
                    _animator.SetBool("Left", false);
                }
                else if(_currentLane.CurrentDirection == "Right")
                {
                    _animator.SetBool("Up", false);
                    _animator.SetBool("Down", false);
                    _animator.SetBool("Right", false);
                    _animator.SetBool("Left", true);
                }
            }


            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, _currentLane.transform.position.z), Time.deltaTime * _playerLaneSwitchingSpeed);
      
        }

        //Set the players lane
        public void SetLane(Lane lane, int number)
        {
            _currentLaneNumber = number;
            _currentLane = lane;
			SFXManager.PlaySound ("Move");
        }


        private void UseFireWall()
        {
            if (Firewall <= 0) return;

        }

        private void UseDataShield()
        {
            if (DataShield <= 0) return;
            _shielded = true;
            DataShield--;
            _boardManager.UiManager.SetDataShield(DataShield);
			SFXManager.PlaySound ("ShieldUse");
        }


    
        // Hit the stuff
        void OnCollisionEnter(Collision collision)
        {
            foreach (ContactPoint contact in collision.contacts)
            {
                Debug.DrawRay(contact.point, contact.normal, Color.white);
            }
            Debug.Log(collision.transform.name);

            // monster switch - don't judge me :)
            switch (collision.transform.tag)
            {
                case "FireWall":
					Instantiate (Resources.Load ("EffectRing"), collision.gameObject.transform.position, new Quaternion (0, 0, 0, 0));
                    Firewall++;
                    Destroy(collision.gameObject);
                    _boardManager.UiManager.SetFirewall(Firewall);
					SFXManager.PlaySound ("PickupCollect");

                    break;
                case "DataShield":
					Instantiate (Resources.Load ("EffectRing"), collision.gameObject.transform.position, new Quaternion (0, 0, 0, 0));
                    DataShield++;
                    Destroy(collision.gameObject);
                    _boardManager.UiManager.SetDataShield(DataShield);
					SFXManager.PlaySound ("PickupCollect");

                    break;
                case "Bullet":
					Instantiate (Resources.Load ("EffectRing"), collision.gameObject.transform.position, new Quaternion (0, 0, 0, 0));
                    Bullet++;
                    Destroy(collision.gameObject);
                    _boardManager.UiManager.SetBullet(Bullet);
					SFXManager.PlaySound ("PickupCollect");

                    break;
                case "Worm":
               
					Instantiate (Resources.Load ("VirusDestroyedEffect"), collision.gameObject.transform.position, new Quaternion (0, 0, 0, 0));
                    Destroy(collision.gameObject);
                    _boardManager.UiManager.SetVirusDestroyed();
                    break;
                case "Trojan":
             
					Instantiate (Resources.Load ("VirusDestroyedEffect"), collision.gameObject.transform.position, new Quaternion (0, 0, 0, 0));
                    Destroy(collision.gameObject);
                    _boardManager.UiManager.SetVirusDestroyed();
                    break;
                case "Malware":
            
					Instantiate (Resources.Load ("VirusDestroyedEffect"), collision.gameObject.transform.position, new Quaternion (0, 0, 0, 0));
                    Destroy(collision.gameObject);
                    _boardManager.UiManager.SetVirusDestroyed();
                    break;
                case "Spam":
					Instantiate (Resources.Load ("VirusDestroyedEffect"), collision.gameObject.transform.position, new Quaternion (0, 0, 0, 0));
                    Destroy(collision.gameObject);
                    _boardManager.UiManager.SetVirusDestroyed();
                    break;
                case "CatPhoto":
                    if (_shielded)
                    {
                        collision.collider.isTrigger = true;
                        _shielded = false;
                    }
                    else
                    {
						Instantiate (Resources.Load ("CatDestroyedEffect"), collision.gameObject.transform.position, new Quaternion (0, 0, 0, 0));
                        Destroy(collision.gameObject);
                        _boardManager.UiManager.DataCorrupted();
                    }
                    break;
                case "Like":
                    if (_shielded)
                    {
                        collision.collider.isTrigger = true;
                        _shielded = false;
                    }
                    else
                    {
						Instantiate (Resources.Load ("LikeDestroyedEffect"), collision.gameObject.transform.position, new Quaternion (0, 0, 0, 0));	
                        Destroy(collision.gameObject);
                        _boardManager.UiManager.DataCorrupted();
                    }
                    break;
                case "Email":
                    if (_shielded)
                    {
                        collision.collider.isTrigger = true;
                        _shielded = false;
                    }
                    else
                    {
						Instantiate (Resources.Load ("EmailDestroyedEffect"), collision.gameObject.transform.position, new Quaternion (0, 0, 0, 0));
                        Destroy(collision.gameObject);
                        _boardManager.UiManager.DataCorrupted();
                    }
                    break;
                case "ShoppingCart":
                    if (_shielded)
                    {
                        collision.collider.isTrigger = true;
                        _shielded = false;
                    }
                    else
                    {
						Instantiate (Resources.Load ("ShoppingCartDestroyEffect"), collision.gameObject.transform.position, new Quaternion (0, 0, 0, 0));
                        Destroy(collision.gameObject);
                        _boardManager.UiManager.DataCorrupted();
                    }
                    break;
                case "Data":
                    Destroy(collision.gameObject);
                    break;
            }
        }
    }
}
