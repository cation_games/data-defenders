﻿using UnityEngine;

namespace Assets.Scripts
{
    //Power up firewall - diesafter a while, kills virus it touches
    public class FireWall : MonoBehaviour
    {
        private BoardManager _boardManager;

        public float SecondsToLive = 30;
        // Use this for initialization
        void Start()
        {
            _boardManager = FindObjectOfType<BoardManager>();
        }

        // Update is called once per frame
        void Update()
        {
            transform.localScale = new Vector3(SecondsToLive / 8, 1.5f,2.2f);
            SecondsToLive -= 0.02f;
            if (SecondsToLive <= 0)
                Destroy(this.gameObject);
        }

        void OnTriggerEnter(Collider collision)
        {


            // baby monster switch - don't judge me :)
            switch (collision.transform.tag)
            {
                case "Worm":
					Instantiate (Resources.Load ("VirusDestroyedEffect"), collision.gameObject.transform.position, new Quaternion (0, 0, 0, 0));
                    Destroy(collision.gameObject);
                    _boardManager.UiManager.SetVirusDestroyed();
                    break;
                case "Trojan":
					Instantiate (Resources.Load ("VirusDestroyedEffect"), collision.gameObject.transform.position, new Quaternion (0, 0, 0, 0));
                    Destroy(collision.gameObject);
                    _boardManager.UiManager.SetVirusDestroyed();
                    break;
                case "Malware":
					Instantiate (Resources.Load ("VirusDestroyedEffect"), collision.gameObject.transform.position, new Quaternion (0, 0, 0, 0));
                    Destroy(collision.gameObject);
                    _boardManager.UiManager.SetVirusDestroyed();
                    break;
                case "Spam":
					Instantiate (Resources.Load ("VirusDestroyedEffect"), collision.gameObject.transform.position, new Quaternion (0, 0, 0, 0));
                    Destroy(collision.gameObject);
                    _boardManager.UiManager.SetVirusDestroyed();
                    break;

            }
        }
    }
}
