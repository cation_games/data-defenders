﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    // Quit and start
   
    public class MainMenu : MonoBehaviour
    {

        public GameObject credits;
        public void QuitGame()
        {
            Debug.Log("QUIT");
            Application.Quit();

        }

        public void  OpenCredits()
        {
            credits.SetActive(true);
            this.gameObject.SetActive(false);
        }



        public void StartGame()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

        }

        void Update()
        {
            if (Input.GetButtonDown("JoystickX"))
                StartGame();
            else if (Input.GetButtonDown("JoystickY"))
                OpenCredits();
            else if (Input.GetButtonDown("JoystickB"))
                QuitGame();
        }
    }
}
