﻿using UnityEngine;

namespace Assets.Scripts
{
    // Holds current lane direction
    public class Lane : MonoBehaviour
    {
        public string CurrentDirection;
        // Use this for initialization
        void Start ()
        {
            CurrentDirection = "Left";
        }
	
        // Update is called once per frame
        void Update () {
		
        }

        public void ChangeDirection()
        {
            if (CurrentDirection == "Left")
                CurrentDirection = "Right";
            else if (CurrentDirection == "Right")
                CurrentDirection = "Left";
        
        }
    }
}
