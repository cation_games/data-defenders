﻿using UnityEngine;

namespace Assets.Scripts
{
    // Bang bang I shot you down
    public class Bullet : MonoBehaviour
    {
        private BoardManager _boardManager;
        public float MoveSpeed = 4;
        // Use this for initialization
        void Start()
        {
            _boardManager = FindObjectOfType<BoardManager>();
        }

        // Update is called once per frame
        void Update()
        {
            transform.Translate(MoveSpeed * Time.deltaTime, 0, 0);
            if (transform.position.x < -25 || transform.position.x > 40)
                Destroy(this.gameObject);
        }

        void OnTriggerEnter(Collider collision)
        {
       

            // monster switch - don't judge me :)
            switch (collision.transform.tag)
            {
                case "Worm":
					Instantiate (Resources.Load ("VirusDestroyedEffect"), collision.gameObject.transform.position, new Quaternion (0, 0, 0, 0));
                    Destroy(collision.gameObject);
                    _boardManager.UiManager.SetVirusDestroyed();
                    Destroy(this.gameObject);
                    break;
                case "Trojan":
					Instantiate (Resources.Load ("VirusDestroyedEffect"), collision.gameObject.transform.position, new Quaternion (0, 0, 0, 0));
                    Destroy(collision.gameObject);
                    _boardManager.UiManager.SetVirusDestroyed();
                    Destroy(this.gameObject);
                    break;
                case "Malware":
					Instantiate (Resources.Load ("VirusDestroyedEffect"), collision.gameObject.transform.position, new Quaternion (0, 0, 0, 0));
                    Destroy(collision.gameObject);
                    _boardManager.UiManager.SetVirusDestroyed();
                    Destroy(this.gameObject);
                    break;
                case "Spam":
					Instantiate (Resources.Load ("VirusDestroyedEffect"), collision.gameObject.transform.position, new Quaternion (0, 0, 0, 0));
                    Destroy(collision.gameObject);
                    _boardManager.UiManager.SetVirusDestroyed();
                    Destroy(this.gameObject);
                    break;
            }
        }
    }
}
