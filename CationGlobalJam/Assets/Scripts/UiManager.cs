﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class UiManager : MonoBehaviour
    {                                                                      
        [SerializeField] private Animator _firewallNumber;
        [SerializeField] private Animator _dataShieldNumber;
        [SerializeField] private Animator _bulletNumber;
        [SerializeField] private Animator _firewallPulse;
        [SerializeField] private Animator _dataShieldPulse;
        [SerializeField] private Animator _bulletPulse;

        [SerializeField] private Animator _virusDestroyed;
        [SerializeField] private Animator _virusDestroyedPulse;
        public int _virusDestroyedNumber;
        [SerializeField] private Animator _dataCorrupted;
        [SerializeField] private Animator _dataCorruptedPulse;
        public int _dataCorruptedNumber;
        [SerializeField] private Animator _virusPassed;
        [SerializeField] private Animator _virusPassedPulse;
        public int _virusPassedNumber;

        [SerializeField] private Sprite _usingFirewalls;
        [SerializeField] private Sprite _moving;
        [SerializeField] private Sprite _destroyViruses;
        [SerializeField] private Sprite _corruptedData;
        [SerializeField] private Sprite _backUp;
        [SerializeField] private Sprite _bullet;
        [SerializeField] private Sprite _activeVirus;
        [SerializeField] private Animator _popUp;
        [SerializeField] private Text _scoreText;


        public AudioSource audioSOurce;
        public int _score;
        // Checks for tutorial popup
        private bool _firstFirewall = true;
        private bool _firstDataShield = true;
        private bool _firstBullet = true;
        private bool _firstDataCorrupted = true;
        private bool _firstVirusDestroyed = true;
        private bool _firstVirusPassed = true;

        private bool first;
        // Use this for initialization
        void Start ()
        {
            OpenPopUp(_moving);
           
        }

        //Close the pop up
        public void ClosePopUp()
        {
            _popUp.SetBool("Open", false);
            Time.timeScale = 1.0f;
            if (!first)
            {
                first = true;
                audioSOurce.Play();
            }
            else
                audioSOurce.UnPause();
        }

        void Update()
        {
            if(Input.GetButton("JoystickA"))
            {
                ClosePopUp();
            }
        }


        //Close the pop up
        public void OpenPopUp(Sprite sprite)
        {
            _popUp.SetBool("Open", true);
            _popUp.GetComponent<Image>().sprite = sprite;
            Time.timeScale = 0.0f;
            audioSOurce.Pause();
        }

        // Set how much data has been destroyed
        public void VirusPassed()
        {

            if (_firstVirusPassed)
            {
                _firstVirusPassed = false;
                OpenPopUp(_activeVirus);
            }
            _virusPassedNumber++;
            SetScore(-4);
            _virusPassed.GetComponentInChildren<Text>().text = _virusPassedNumber.ToString();
            _virusPassed.SetTrigger("Pulse");
            _virusPassedPulse.SetTrigger("Pulse");
			SFXManager.PlaySound ("VirusSuccess");
        }

        // Set how much data has been destroyed
        public void DataCorrupted()
        {
            if (_firstDataCorrupted)
            {
                _firstDataCorrupted = false;
                OpenPopUp(_corruptedData);
            }
            SetScore(-2);
            _dataCorruptedNumber++;
            _dataCorrupted.GetComponentInChildren<Text>().text = _dataCorruptedNumber.ToString();
            _dataCorrupted.SetTrigger("Pulse");
            _dataCorruptedPulse.SetTrigger("Pulse");
			SFXManager.PlaySound ("DestroyedData");
        }

        // Set how many viruses have been destroyed
        public void SetVirusDestroyed()
        {
            if (_firstVirusDestroyed)
            {
                _firstVirusDestroyed = false;
                OpenPopUp(_destroyViruses);
            }
            SetScore(1);
            _virusDestroyedNumber++;
            _virusDestroyed.GetComponentInChildren<Text>().text = _virusDestroyedNumber.ToString();
            _virusDestroyed.SetTrigger("Pulse");
            _virusDestroyedPulse.SetTrigger("Pulse");
			SFXManager.PlaySound ("DestroyedVirus");
        }


        // Set how many viruses have been destroyed
        public void DataSurvived()
        {
            SetScore(1);
          
        }

        // Update the power ups number
        public void SetPowerUps(Player player)
        {
            _firewallNumber.GetComponentInChildren<Text>().text = player.Firewall.ToString();
            _dataShieldNumber.GetComponentInChildren<Text>().text = player.DataShield.ToString();
            _bulletNumber.GetComponentInChildren<Text>().text = player.Bullet.ToString();
            _firewallNumber.SetTrigger("Pulse");
            _dataShieldNumber.SetTrigger("Pulse");
            _bulletNumber.SetTrigger("Pulse");
            _bulletPulse.SetTrigger("Pulse");
            _firewallPulse.SetTrigger("Pulse");
            _dataShieldPulse.SetTrigger("Pulse");
        }

        // Update the power ups number
        public void SetFirewall(int number)
        {
            if (_firstFirewall)
            {
                _firstFirewall = false;
                OpenPopUp(_usingFirewalls);
            }
            _firewallNumber.GetComponentInChildren<Text>().text = number.ToString();
            _firewallNumber.SetTrigger("Pulse");
            _firewallPulse.SetTrigger("Pulse");
        }

        // Update the power ups number
        public void SetDataShield(int number)
        {
            if (_firstDataShield)
            {
                _firstDataShield = false;
                OpenPopUp(_backUp);
            }
            _dataShieldNumber.GetComponentInChildren<Text>().text = number.ToString();
            _dataShieldNumber.SetTrigger("Pulse");
            _bulletPulse.SetTrigger("Pulse");
        }


        // Update the power ups number
        public void SetBullet(int number)
        {
            if (_firstBullet)
            {
                _firstBullet = false;
                OpenPopUp(_bullet);
            }
            _bulletNumber.GetComponentInChildren<Text>().text = number.ToString();
            _bulletNumber.SetTrigger("Pulse");
            _dataShieldPulse.SetTrigger("Pulse");
        }

        public void SetScore(int change)
        {
            _score += change;
            if (_score < 0)
                _score = 0;
            _scoreText.text = "Score: " + _score * 100;
        }
    }
}
