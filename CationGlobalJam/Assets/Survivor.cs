﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Survivor : MonoBehaviour {

    public float finalScore;
    public float DataCorrupted;
    public float VirusPassed;
    public float VirusDestroyed;
    private UiManager uiManager;
    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }
    // Use this for initialization
    void Start ()
    {
        uiManager.GetComponent<UiManager>();

    }
	
	// Update is called once per frame
	void Update () {
	    if (SceneManager.GetActiveScene().buildIndex == 1)
	    {
	        finalScore = uiManager._score;
            DataCorrupted = uiManager._dataCorruptedNumber;
            VirusPassed = uiManager._virusPassedNumber;
            VirusDestroyed = uiManager._virusDestroyedNumber;
        }
    }
}
